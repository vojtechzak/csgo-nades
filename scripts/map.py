#!/usr/bin/env python3

import os, sys
from templater import replace, write, copy_images, load_map_desrc, load_nade_descr
from templater import MAP_HTML, SMOKE_STATIC_LI, SMOKE_MOVING_LI

def main(map):
	
	descr = load_map_desrc(map)
	descr['SMOKES'] = get_smokes_list(f'{map}/smokes')

	content = replace(MAP_HTML, descr)
	write(f'{map}/{map}.html', content)


def get_smokes_list(folder):

	copy_images(folder)
	content = []

	if not os.path.exists(f'maps/{folder}'):
		return ''

	for smoke in os.listdir(f'maps/{folder}'):

		descr = load_nade_descr(folder, smoke)
		print(descr)
		moving = 'THROW' in descr
		file = SMOKE_MOVING_LI if moving else SMOKE_STATIC_LI

		content.append(replace(file, descr))

	return '\n'.join(content)


if __name__ == '__main__':
	map = sys.argv[1]
	main(map)
