#!/usr/bin/env python3

import os
from templater import replace, write, load_map_desrc
from templater import INDEX_HTML, MAP_LI_HTML

def main():

	maps = [os.path.basename(f) for f in os.listdir('maps')]
	list = get_maps_list(maps)

	content = replace(INDEX_HTML, {'MAPS': list})
	write(INDEX_HTML, content)


def get_maps_list(maps):

	items = [replace(MAP_LI_HTML, load_map_desrc(map)) for map in maps]
	return '\n'.join(items)


if __name__ == '__main__':
	main()
