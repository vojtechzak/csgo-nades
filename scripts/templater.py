#!/usr/bin/env python3

import os, sys, shutil
from shutil import copytree, ignore_patterns

INDEX_HTML = 'index.html'
MAP_LI_HTML = 'map_li.html'
MAP_HTML = 'map.html'
SMOKE_STATIC_LI = 'smoke_static_li.html'
SMOKE_MOVING_LI = 'smoke_moving_li.html'


def replace(src, dict):

	with open(f'templates/{src}', 'r') as src_file:
		content = src_file.read()

	for k, v in dict.items():
		content = content.replace(f'${k}', v)

	return content


def write(target, content):
	
	with open(f'public/{target}', 'w+') as file:
		file.write(content)


def copy_images(src):

	if not os.path.exists(f'maps/{src}'):
		print(f'maps/{src} not found', file=sys.stderr)
		return ''

	shutil.copytree(f'maps/{src}', f'public/{src}', ignore=ignore_patterns('*.txt'))


def load_map_desrc(map):

	descr = load_description(f'maps/{map}/{map}.txt')
	descr['CODE'] = map
	return descr


def load_nade_descr(folder, nade):

	descr = load_description(f'maps/{folder}/{nade}/{nade}.txt')
	descr['CODE'] = nade
	return descr


def load_description(src):

	if not os.path.exists(src):
		print(f'{src} not found', file=sys.stderr)
		return {}

	descr = {}
	with open(src, 'r') as file:
		for line in file.readlines():
			key, value = line.split(': ')
			descr[key] = value.strip()
	return descr
