#!/usr/bin/env bash

rm -rf public
mkdir -p public

cp sources/* public
python3 scripts/index.py

for map in maps/*
do
    name=$(basename $map)
    mkdir -p public/$name
    cp maps/$name/$name.jpg public/$name
    python3 scripts/map.py $name
done
